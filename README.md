[![Latest Release](https://gitlab.com/abisszett/iosevka-1001/-/badges/release.svg)](https://gitlab.com/abisszett/iosevka-1001/-/releases) [![pipeline status](https://gitlab.com/abisszett/iosevka-1001/badges/main/pipeline.svg)](https://gitlab.com/abisszett/iosevka-1001/-/pipelines)

# Iosevka-1001:
## As the name might suggest, the focus is on confusing characters like 0 vs O and 1 vs l.
## Custom font based on [Iosevka](https://github.com/be5invis/Iosevka) with mainly the following changes:

```toml
l = "serifed-flat-tailed"
zero = "tall-slashed"
one = "base"
brace = "straight"
percent = "rings-continuous-slash"
bar = "force-upright"
```
## Other changes can be seen in the [build file.](private-build-plans.toml)
## This repo pulls upstream code and (re)builds the font incorporating the custom changes.
## The latest build can be downloaded from [here](https://gitlab.com/abisszett/iosevka-1001/-/jobs/artifacts/main/download?job=build)
